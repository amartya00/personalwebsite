#ifndef __SIGABRT_PERSONALSITE_HANDLERFACTORY__
#define __SIGABRT_PERSONALSITE_HANDLERFACTORY__

#include <proxygen/httpserver/RequestHandlerFactory.h>
#include <proxygen/httpserver/RequestHandler.h>

using proxygen::RequestHandlerFactory;
using proxygen::RequestHandler;
using proxygen::HTTPMessage;
using folly::EventBase;

namespace Sigabrt {
    namespace Server {
        class MainRequestHandlerFactory : public RequestHandlerFactory {
            void onServerStart (EventBase*) noexcept override;
            void onServerStop() noexcept override;
            RequestHandler* onRequest (RequestHandler*, HTTPMessage*) noexcept override;
        };
    }
}

#endif
