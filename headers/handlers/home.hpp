#ifndef __SIGABRT_PERONALSITE_HOME__
#define __SIGABRT_PERONALSITE_HOME__

#include <folly/Memory.h>
#include <proxygen/httpserver/RequestHandler.h>
#include <proxygen/httpserver/ResponseHandler.h>

using proxygen::RequestHandler;
using proxygen::HTTPMessage;
using folly::IOBuf;
using proxygen::UpgradeProtocol;
using proxygen::ProxygenError;

namespace Sigabrt {
    namespace Handlers {
        class HomeHandler : public RequestHandler {
            public:
                explicit HomeHandler();
                void onRequest(std::unique_ptr<HTTPMessage> headers) noexcept override;
                void onBody(std::unique_ptr<IOBuf> msgBody) noexcept override;
                void onEOM() noexcept override;
                void onUpgrade(UpgradeProtocol proto) noexcept override;
                void requestComplete() noexcept override;
                void onError(ProxygenError err) noexcept override;
        private:
            std::unique_ptr<IOBuf> body;
        };
    }
}

#endif
