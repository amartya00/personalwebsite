#include <handlers/home.hpp>

#include <folly/Memory.h>
#include <proxygen/httpserver/RequestHandler.h>
#include <proxygen/httpserver/ResponseBuilder.h>
#include <folly/portability/Unistd.h>

using folly::IOBuf;
using proxygen::HTTPMessage;
using proxygen::ResponseBuilder;
using Sigabrt::Handlers::HomeHandler;

HomeHandler::HomeHandler() {
}

void HomeHandler::onRequest(std::unique_ptr<HTTPMessage> /*headers*/) noexcept {
    std::cout << "Request received\n";
}

void HomeHandler::onBody(std::unique_ptr<folly::IOBuf> msgBody) noexcept {
  if (body) {
      std::cout << "Called\n";
      body->prependChain(std::move(msgBody));
  } else {
      body = std::move(msgBody);
  }
}

void HomeHandler::onEOM() noexcept {
  ResponseBuilder(downstream_)
    .status(200, "OK")
    .header("Request-Number", "1")
    .body(std::move(body))
    .sendWithEOM();
}

void HomeHandler::onUpgrade(UpgradeProtocol /*proto*/) noexcept {
  // handler doesn't support upgrades
}

void HomeHandler::requestComplete() noexcept {
  delete this;
}

void HomeHandler::onError(ProxygenError /*err*/) noexcept { 
    delete this;
}

