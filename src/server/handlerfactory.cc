#include <server/handlerfactory.hpp>
#include <handlers/home.hpp>

#include <folly/Memory.h>
#include <proxygen/httpserver/RequestHandler.h>


using folly::EventBase;
using proxygen::RequestHandler;
using proxygen::HTTPMessage;

using Sigabrt::Server::MainRequestHandlerFactory;
using Sigabrt::Handlers::HomeHandler;

void MainRequestHandlerFactory::onServerStart(EventBase* /*evb*/) noexcept {
    // Do nothing
}

void MainRequestHandlerFactory::onServerStop() noexcept {
    // Do nothing
}

RequestHandler* MainRequestHandlerFactory::onRequest (RequestHandler*, HTTPMessage*) noexcept {
    std::cout << "Request\n";
    return new HomeHandler();
}





