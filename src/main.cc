#include <iostream>
#include <string>
#include <vector>
#include <thread>

#include <folly/portability/GFlags.h>
#include <folly/portability/Unistd.h>
#include <folly/io/async/EventBaseManager.h>
#include <proxygen/httpserver/RequestHandlerFactory.h>
#include <proxygen/httpserver/ResponseBuilder.h>
#include <proxygen/httpserver/HTTPServer.h>

#include <server/handlerfactory.hpp>

using folly::SocketAddress;
using proxygen::HTTPServerOptions;
using proxygen::RequestHandlerChain;
using Sigabrt::Server::MainRequestHandlerFactory;
using proxygen::HTTPServer;

using IPConfig = proxygen::HTTPServer::IPConfig;
using Protocol = proxygen::HTTPServer::Protocol;

// Define the flags
DEFINE_int32(httpport, 8080, "HTTP port to listen to");
DEFINE_string(ip, "localhost", "Hostname to bind to");

int main(int argc, char* argv[]) {
    gflags::ParseCommandLineFlags(&argc, &argv, true);
    google::InitGoogleLogging(argv[0]);
    google::InstallFailureSignalHandler();

    std::uint16_t httpPort {static_cast<std::uint16_t>(FLAGS_httpport)};
    std::string ip {FLAGS_ip};

    IPConfig httpConfig {SocketAddress(ip, httpPort, true), Protocol::HTTP};

    std::vector<IPConfig> ipconfigs {};
    ipconfigs.push_back(httpConfig);

    HTTPServerOptions options;
    options.threads = sysconf(_SC_NPROCESSORS_ONLN);
    options.idleTimeout = std::chrono::milliseconds(60000);
    options.shutdownOn = {SIGINT, SIGTERM};
    options.enableContentCompression = false;
    options.handlerFactories = RequestHandlerChain()
      .addThen<MainRequestHandlerFactory>()
      .build();
    options.initialReceiveWindow = uint32_t(1 << 20);
    options.receiveStreamWindowSize = uint32_t(1 << 20);
    options.receiveSessionWindowSize = 10 * (1 << 20);
    options.h2cEnabled = true;

    HTTPServer server(std::move(options));
    server.bind(ipconfigs);

    std::thread thd([&]() {
        server.start();
    });
    thd.join();
}

